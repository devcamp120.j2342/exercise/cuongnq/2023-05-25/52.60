package model;

public class user {
    private String password;
    private String userName;
    private boolean Enable;
    public user() {
    }
    public user(String password, String userName, boolean enable) {
        this.password = password;
        this.userName = userName;
        Enable = enable;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public boolean isEnable() {
        return Enable;
    }
    public void setEnable(boolean enable) {
        Enable = enable;
    }
    @Override
    public String toString() {
        return "user [password=" + password + ", userName=" + userName + ", Enable=" + Enable + "]";
    }
}
